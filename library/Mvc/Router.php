<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   mvc_example
 * @version   1.0.0
 * @since     2014.11.16.
 */

namespace Mvc;

use Mvc\Config;

class Router {

    /**
     * @var Request
     */
    protected $request = null;

    /**
     * @var Config
     */
    protected $config = null;

    /**
     * @var bool
     */
    protected $routed = false;

    /**
     * @var string
     */
    protected $actionName = null;

    /**
     * @var string
     */
    protected $controllerName = null;

    /**
     * @var array
     */
    protected $params = null;

    /**
     * Constructor
     */
    public function __construct() {

    }

    /**
     * Returns action name
     *
     * @return string
     */
    public function getActionName() {
        return $this->actionName;
    }

    /**
     * Returns controller name
     *
     * @return string
     */
    public function getControllerName() {
        return $this->controllerName;
    }

    /**
     * Sets up action name
     *
     * @param string $name
     *
     * @return $this
     */
    public function setActionName($name) {
        $this->actionName = $this->prepare($name);

        return $this;
    }

    /**
     * Sets up controller name
     *
     * @param string $name
     *
     * @return $this
     */
    public function setControllerName($name) {
        $this->controllerName = $this->prepare($name);

        return $this;
    }

    /**
     * Returns params
     *
     * @return array
     */
    public function getParams() {
        return $this->params;
    }

    /**
     * Sets up params
     *
     * @param array $params
     *
     * @return $this
     */
    public function setParams(array $params) {
        $this->params = $params;

        return $this;
    }

    /**
     * Sets up request
     *
     * @param Request $request
     *
     * @return $this
     */
    public function setRequest(Request $request) {
        $this->request = $request;

        return $this;
    }

    /**
     * Returns request
     *
     * @return Request
     */
    public function getRequest() {
        return $this->request;
    }

    /**
     * Route request
     *
     * @param bool $force
     *
     * @return $this
     */
    public function route($force = false) {
        if (true === $force or false === $this->routed) {
            $this->_route();
        }

        return $this;
    }

    /**
     * @param Config $config
     *
     * @return $this
     */
    public function setConfig(Config $config) {
        $this->config = $config;

        return $this;
    }

    /**
     * @return Config
     */
    public function getConfig() {
        return $this->config;
    }

    /**
     * @param string $text
     *
     * @return mixed
     */
    protected function prepare($text) {
        return preg_replace('~[^a-z]~i', '', strtolower($text));
    }


    /**
     * Default route roule
     *
     * @todo
     */
    protected function _route() {
        $uri = trim($this->getRequest()->getServer(Request::URI, '/'), '/');
        $uriArray = explode('/', $uri);
        $params = array();

        if ($this->getConfig()->has($uri)) {
            $this->setControllerName($this->getConfig()->get($uri)->get('controller'));

            if ($this->getConfig()->get($uri)->has('action')) {
                $this->setActionName($this->getConfig()->get($uri)->get('action'));
            }
        } else {
            if (count($uriArray) > 0) {
                $this->setControllerName(array_shift($uriArray));
            }

            if (count($uriArray) > 0) {
                $this->setActionName(array_shift($uriArray));
            }
        }

        while (count($uriArray) > 0) {
            $name = array_shift($uriArray);
            $value = null;

            if (count($uriArray) > 0) {
                $value = array_shift($uriArray);
            }

            $params[$name] = $value;
        }

        $this->setParams($params);
    }
}
