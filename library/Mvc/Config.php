<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   mvc_example
 * @version   1.0.0
 * @since     2014.11.16.
 */

namespace Mvc;


class Config {
    /**
     * @var array config values
     */
    protected $config = array();

    /**
     * @param string|array $aConfig config file path or config value
     * @throws \Exception
     */
    public function __construct($config) {
        if (is_string($config)) {
            $this->config = include($config);
        } elseif (is_array($config)) {
            $this->config = $config;
        } else {
            // FIXME: saját error objektum (?)
            throw new \Exception('Invalid parameter type.');
        }
    }

    public function has($name) {
        return array_key_exists($name, $this->config);
    }

    /**
     * @param string $name
     * @param mixed  $default
     *
     * @return Config|string|number|bool
     */
    public function get($name, $default = null) {
        if ($this->has($name)) {
            if (is_array($this->config[$name])) {
                $default = new Config($this->config[$name]);
            } else {
                $default = $this->config[$name];
            }
        }

        return $default;
    }

    /**
     * @return array config values
     */
    public function toArray() {
        return $this->config;
    }
}
