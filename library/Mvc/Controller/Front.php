<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   mvc_example
 * @version   1.0.0
 * @since     2014.11.16.
 */

namespace Mvc\Controller;

use Mvc\Controller;
use Mvc\Controller\Action as ActionController;
use Mvc\Config;
use Mvc\View;
use Mvc\View\Layout;
use Mvc\Router;

class Front extends Controller {
    const SUFFIX_CONTROLLER = 'Controller';

    const SUFFIX_ACTION = 'Action';

    protected $runed = false;

    /**
     * @var Front
     */
    protected static $instance = null;

    /**
     * @var Config
     */
    protected $config = null;

    /**
     * @var Router
     */
    protected $router = null;

    /**
     * @var ActionController
     */
    protected $controller = null;

    /**
     * constructor
     */
    public function __construct($name = null) {
        parent::__construct('fromtController');
    }

    /**
     * @param mixed $config
     *
     * @return $this
     */
    public function setConfig($config) {
        $this->config = new Config($config);

        return $this;
    }

    /**
     * @return Config
     */
    public function getConfig() {
        return $this->config;
    }

    public function getRouter() {
        if (null === $this->router) {
            $this->router = new Router();
            $this->router->setRequest($this->getRequest())->setConfig($this->getConfig()->get('router', array()));
        }

        return $this->router;
    }

    /**
     * Start application
     */
    public function run() {
        $this->runed = true;
        $actionFnName = $this->getRouter()->route(true)->getActionName() . self::SUFFIX_ACTION;

        if (method_exists($this->getController(), $actionFnName)) {
            $this->getController()->$actionFnName();
        }

        $this->getResponse()->setLayout(
             new Layout()
        )->getLayout()->setConfig($this->config->get('layout'))->setContent($this->getController()->getView());
    }

    /**
     * @return ActionController
     */
    public function getController() {
        if (null === $this->controller) {
            $this->setController();
        }

        return $this->controller;
    }

    /**
     *
     */
    public function __destruct() {
        if (false === $this->runed) {
            $this->run();
        }

        $this->getResponse()->terminate();
    }

    /**
     * @param string $name
     *
     * @return View
     */
    protected function getView($name) {
        $viewFile = $this->config->get('viewPath') . '/'
                    . $this->getController()->getName() . '/'
                    . $name .
                    $this->config->get('viewSuffix');

        return new View($viewFile);
    }

    /**
     * @return $this
     */
    protected function setController() {
        $controllerName = $this->getRouter()->route()->getControllerName();
        $actionName = $this->getRouter()->getActionName();

        if (!$actionName) {
            $actionName = $this->config->get('default')->get('action');
        }

        if (!$controllerName) {
            $controllerName = $this->config->get('default')->get('controller');
        }

        $controllerClassName = ucfirst($controllerName) . self::SUFFIX_CONTROLLER;

        $this->controller = new $controllerClassName($controllerName);
        $this->controller->setView($this->getView($actionName));

        if ($this->getConfig()->has($controllerName . self::SUFFIX_CONTROLLER)) {
            $this->controller->setConfig($this->getConfig()->get($controllerName . self::SUFFIX_CONTROLLER), array());
        }

        return $this;
    }

    /**
     * Returns Request instance.
     *
     * @static
     * @return Request
     */
    public static function getInstance() {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}
