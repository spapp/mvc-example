<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   mvc_example
 * @version   1.0.0
 * @since     2014.11.16.
 */

namespace Mvc\Controller;

use \Mvc\View;
use \Mvc\Controller;

class Action extends Controller {
    /**
     * @var View
     */
    protected $view = null;

    /**
     * @return View
     */
    public function getView() {
        return $this->view;
    }

    /**
     * @param View $view
     *
     * @return $this
     */
    public function setView(View $view) {
        $this->view = $view;

        return $this;
    }
}
