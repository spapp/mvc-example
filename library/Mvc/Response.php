<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   mvc_example
 * @version   1.0.0
 * @since     2014.11.16.
 */

namespace Mvc;

use Mvc\View\Layout;

class Response {
    /**
     * @var Response
     */
    protected static $instance = null;

    /**
     * @var bool
     */
    protected $terminated = false;

    /**
     * @var Layout
     */
    protected $layout = null;

    /**
     * @var Controller
     */
    protected $controller = null;

    /**
     *
     */
    protected function __construct() {

    }

    /**
     * @param Layout $layout
     *
     * @return $this
     */
    public function setLayout(Layout $layout) {
        $this->layout = $layout;

        return $this;
    }

    /**
     * @return Layout
     */
    public function getLayout() {
        return $this->layout;
    }

    /**
     * Terminate response, send data to output
     */
    public function terminate() {
        $this->terminated = true;

        echo (string)$this->getLayout();
    }

    /**
     * @return Response
     */
    public static function getInstance() {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}
