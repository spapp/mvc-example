<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   mvc_example
 * @version   1.0.0
 * @since     2014.11.16.
 */

namespace Mvc;

use Mvc\Config;

class Controller {
    /**
     * @var string
     */
    protected $name = null;

    /**
     * @var Config
     */
    protected $config = null;

    /**
     * @param string $name
     */
    public function __construct($name) {
        $this->setName($name);

        $this->init();
    }

    public function init() {
    }

    /**
     * @param $name
     *
     * @return $this
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * @return null
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param Config $config
     *
     * @return $this
     */
    public function setConfig(Config $config) {
        $this->config = $config;

        return $this;
    }

    /**
     * @return Config
     */
    public function getConfig() {
        return $this->config;
    }

    /**
     * @return Request
     */
    public function getRequest() {
        return Request::getInstance();
    }

    /**
     * @return Response
     */
    public function getResponse() {
        return Response::getInstance();
    }
}
