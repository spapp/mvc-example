<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   mvc_example
 * @version   1.0.0
 * @since     2014.11.16.
 */

namespace Mvc;


class View {
    /**
     * @var null template filename
     */
    protected $fileName = null;

    /**
     * @var array template variables container
     */
    protected $params = array();

    /**
     * @var Config
     */
    protected $config = null;

    /**
     * @param string $fileName
     */
    public function __construct($fileName = null) {
        if (null !== $fileName) {
            $this->setFileName($fileName);
        }
    }

    /**
     * @param Config $config
     *
     * @return $this
     */
    public function setConfig(Config $config) {
        $this->config = $config;

        if ($this->config->has('fileName')) {
            $this->setFileName($this->config->get('fileName'));
        }

        return $this;
    }

    /**
     * @return Config
     */
    public function getConfig() {
        return $this->config;
    }

    /**
     * @param string $fileName
     *
     * @return $this
     */
    public function setFileName($fileName) {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @return string
     */
    public function getFileName() {
        return $this->fileName;
    }

    /**
     * @param string $name
     * @param mixed  $value
     *
     * @return $this
     */
    public function set($name, $value) {
        $this->params[$name] = $value;

        return $this;
    }

    /**
     * @param string $name
     * @param mixed  $default
     *
     * @return mixed
     */
    public function get($name, $default = null) {
        if ($this->has($name)) {
            $default = $this->params[$name];
        }

        return $default;
    }

    public function has($name) {
        return isset($this->params[$name]);
    }

    /**
     * @return string
     */
    public function render() {
        ob_start();

        include($this->getFileName());

        $output = ob_get_contents();
        ob_clean();

        return $output;
    }

    /**
     * @param $glue
     * @param $param1
     *
     * @return string
     */
    public function merge($glue, $param1 /* , parma2, parma3, $paramN */) {
        $params = func_get_args();
        $glue = array_shift($params);

        foreach ($params as $i => $value) {
            if (strlen(trim((string)$value)) < 1) {
                unset($params[$i]);
            }
        }

        return implode($glue, $params);
    }

    /**
     * @return string
     */
    public function __toString() {
        return (string)$this->render();
    }
}
