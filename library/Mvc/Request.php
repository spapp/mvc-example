<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   mvc_example
 * @version   1.0.0
 * @since     2014.11.16.
 */

namespace Mvc;


class Request {
    const URI = 'REQUEST_URI';

    const METHOD = 'REQUEST_METHOD';

    const METHOD_POST = 'POST';

    const METHOD_GET = 'GET';

    /**
     * @var Request
     */
    protected static $instance = null;

    /**
     * @var Config
     */
    protected $config = null;

    /**
     *
     */
    protected function __construct() {

    }

    /**
     * Sets request config
     *
     * @param Config $config
     *
     * @return $this
     */
    public function setConfig(Config $config) {
        $this->config = $config;

        return $this;
    }

    /**
     * Returns server variable
     *
     * @param string $name
     * @param mixed  $default
     *
     * @return null
     */
    public function getServer($name, $default = null) {
        if (isset($_SERVER[$name])) {
            $default = $_SERVER[$name];
        }

        return $default;
    }

    /**
     * Returns _TRUE_ if the request method post
     *
     * @return bool
     */
    public function isPost() {
        return (self::METHOD_POST === $this->getServer(self::METHOD));
    }

    /**
     * Returns _TRUE_ if the request method get
     *
     * @return bool
     */
    public function isGet() {
        return (self::METHOD_GET === $this->getServer(self::METHOD));
    }

    public function getPostParam($name, $default = null) {
        if (isset($_POST[$name])) {
            $default = $_POST[$name];
        }

        return $default;
    }

    public function getPostParams() {
        return $_POST;
    }

    /**
     * Returns Request instance.
     *
     * @static
     * @return Request
     */
    public static function getInstance() {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}
