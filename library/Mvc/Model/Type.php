<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   mvc_example
 * @version   1.0.0
 * @since     2014.11.18.
 */

namespace Mvc\Model;

use Mvc\Model\Type\String;
use Mvc\Model\Type\Email;
use Mvc\Config;

class Type {
    const DEFAULT_DATA_TYPE = 'string';

    protected function __construct() {
    }

    public static function factory(Config $config, $value) {
        $instance = null;
        $type = $config->get('type', self::DEFAULT_DATA_TYPE);

        switch (strtolower($type)) {
            case 'string':
                $instance = new String($value);
                break;
            case 'email':
                $instance = new Email($value);
                break;
        }

        $instance->setConfig($config);

        return $instance;
    }
} 