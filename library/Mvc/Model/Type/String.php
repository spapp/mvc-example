<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   mvc_example
 * @version   1.0.0
 * @since     2014.11.18.
 */

namespace Mvc\Model\Type;

use Mvc\Config;
use Mvc\Model\Type\TypeInterface;

class String implements TypeInterface {
    /**
     * @var string
     */
    protected $data = '';

    /**
     * @var int
     */
    protected $minLength = 0;

    /**
     * @var int
     */
    protected $maxLength = PHP_INT_MAX;

    /**
     * @var string
     */
    protected $message = '';

    /**
     * Constructor
     */
    public function __construct($text = '') {
        $this->setData($text);
    }

    /**
     * @param string $message
     *
     * @return $this
     */
    public function setMessage($message) {
        $this->message = $message;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * @return bool
     */
    public function isValid() {
        $length = strlen(trim($this->getData()));

        return ($length >= $this->getMinLength() and $length <= $this->getMaxLength());
    }

    /**
     * @param Config $config
     *
     * @return $this
     */
    public function setConfig(Config $config) {
        $config = $config->toArray();

        foreach ($config as $key => $value) {
            $method = 'set' . ucfirst($key);

            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }

        return $this;
    }

    /**
     * @param string $data
     *
     * @return $this
     */
    public function setData($data) {
        $this->data = $data;

        return $this;
    }

    /**
     * @return string
     */
    public function getData() {
        return $this->data;
    }

    /**
     * @param int $maxLength
     *
     * @return $this
     */
    public function setMaxLength($maxLength) {
        $this->maxLength = $maxLength;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaxLength() {
        return $this->maxLength;
    }

    /**
     * @param int $minLength
     *
     * @return $this
     */
    public function setMinLength($minLength) {
        $this->minLength = $minLength;

        return $this;
    }

    /**
     * @return int
     */
    public function getMinLength() {
        return $this->minLength;
    }


} 