<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   mvc_example
 * @version   1.0.0
 * @since     2014.11.18.
 */

namespace Mvc\Model\Type;

use Mvc\Config;

interface TypeInterface {
    /**
     * @param Config $config
     *
     * @return mixed
     */
    public function setConfig(Config $config);

    /**
     * @return bool
     */
    public function isValid();

    /**
     * @param mixed $data
     *
     * @return mixed
     */
    public function setData($data);

    /**
     * @return mixed
     */
    public function getData();
} 