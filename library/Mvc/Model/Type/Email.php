<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   mvc_example
 * @version   1.0.0
 * @since     2014.11.18.
 */

namespace Mvc\Model\Type;

use Mvc\Config;
use Mvc\Model\Type\TypeInterface;

class Email implements TypeInterface {
    const PATTERN = '~^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*\\.[A-Za-z]{2,}$~';

    /**
     * @var string
     */
    protected $data = '';

    /**
     * @var string
     */
    protected $message = '';

    /**
     * Constructor
     */
    public function __construct($email = '') {
        $this->setData($email);
    }

    /**
     * @return bool
     */
    public function isValid() {
        return (bool)preg_match(self::PATTERN, $this->getData());
    }

    /**
     * @param Config $config
     *
     * @return $this
     */
    public function setConfig(Config $config) {
        $config = $config->toArray();

        foreach ($config as $key => $value) {
            $method = 'set' . ucfirst($key);

            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }

        return $this;
    }

    /**
     * @param string $message
     *
     * @return $this
     */
    public function setMessage($message) {
        $this->message = $message;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * @param string $data
     *
     * @return $this
     */
    public function setData($data) {
        $this->data = $data;

        return $this;
    }

    /**
     * @return string
     */
    public function getData() {
        return $this->data;
    }
}
