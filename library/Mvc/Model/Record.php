<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   mvc_example
 * @version   1.0.0
 * @since     2014.11.18.
 */

namespace Mvc\Model;

use Mvc\Model\Type;
use Mvc\Config;

class Record {
    /**
     * @var Config
     */
    protected $config = null;

    /**
     * @var array
     */
    protected $data = null;

    /**
     * @var array
     */
    protected $lastError = array();

    /**
     * @param array $data
     */
    public function __construct(array $data = null) {
        if (is_array($data)) {
            $this->setData($data);
        }
    }

    /**
     * @param array|Config $config
     *
     * @return $this
     */
    public function setConfig($config) {
        if (is_array($config)) {
            $this->config = new Config($config);
        } else {
            $this->config = $config;
        }

        foreach ($this->config->toArray() as $name => &$value) {
            if (!isset($this->data[$name]) and isset($value['default'])) {
                $this->setData($name, $value['default']);
            }
        }

        return $this;
    }

    /**
     * @return Config
     */
    public function getConfig() {
        return $this->config;
    }

    /**
     * @return bool
     */
    public function isValid() {
        $this->lastError = array();

        foreach ($this->data as $name => &$value) {
            if (false === $value->isValid()) {
                $this->lastError[$name] = $value->getMessage();
            }
        }

        return (count($this->lastError) < 1);
    }

    /**
     * @return array
     */
    public function getLastError() {
        return $this->lastError;
    }

    /**
     * @param array|string $name
     * @param              string [$value]
     *
     * @return $this
     */
    public function setData($name, $value = null) {
        if (!is_array($this->data)) {
            $this->data = array();
        }

        if (is_array($name)) {
            $data = $name;
        } else {
            $data = array();
            $data[$name] = $value;
        }

        foreach ($data as $name => $value) {
            if ($this->getConfig()->has($name)) {
                $this->data[$name] = Type::factory($this->getConfig()->get($name), $value);
            }
        }

        return $this;
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function get($name) {
        return $this->data[$name]->getData();
    }
} 