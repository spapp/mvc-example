<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   mvc_example
 * @version   1.0.0
 * @since     2014.11.16.
 */

namespace Mvc\View;

use Mvc\View;

class Layout extends View {
    /**
     * @var View
     */
    protected $content = null;

    /**
     * @param View $content
     *
     * @return $this
     */
    public function setContent(View $content) {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string
     */
    public function getContent() {
        return $this->content;
    }
}
