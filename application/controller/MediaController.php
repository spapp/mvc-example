<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   mvc_example
 * @version   1.0.0
 * @since     2014.11.16.
 */

use Mvc\Controller\Action as ActionController;

class MediaController extends ActionController {
    public function init(){

    }

    public function videoAction(){
        $this->getView()->set('video', $this->getConfig()->get('video')->toArray());
    }
}
