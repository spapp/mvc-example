<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   mvc_example
 * @version   1.0.0
 * @since     2014.11.16.
 */

use Mvc\Controller\Action as ActionController;
use Mvc\Model\Record;
use Mvc\Config;

class ContactController extends ActionController {
    public function init() {

    }

    public function mailAction() {
        $messageSent = false;
        $messageForm = new Record();

        $messageForm->setConfig($this->getConfig()->get('messageForm'));

        if (true === $this->getRequest()->isPost()) {
            $messageForm->setData($this->getRequest()->getPostParams());

            if (true === $messageForm->isValid()) {
                // send message
                $messageSent = true;
            } else {
                $this->getView()->set('error', new Config($messageForm->getLastError()));
            }
        }

        $this->getView()->set('messageSent', $messageSent);
        $this->getView()->set('messageForm', $messageForm);
    }

    public function mapAction() {
        $this->getView()->set('map', (object)$this->getConfig()->get('map')->toArray());
    }
} 