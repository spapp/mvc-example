<?php

return array(
        'default'           => array(
                'controller' => 'home',
                'action'     => 'index'
        ),
        'layout'            => array(
                'fileName' => APPLICATION_PATH . '/application/view/layout.phtml',
                'title'    => 'Krisna-völgy'
        ),
        'viewPath'          => APPLICATION_PATH . '/application/view',
        'viewSuffix'        => '.phtml',
        'router'            => array(
                ''                => array(
                        'controller' => 'home',
                        'action'     => 'index',
                        'label'      => 'Kezdőlap'
                ),
                'mi-krisna-volgy' => array(
                        'controller' => 'home',
                        'action'     => 'info',
                        'label'      => 'Mi Krisna-völgy?'
                ),
                'kornyekunk'      => array(
                        'controller' => 'home',
                        'action'     => 'region',
                        'label'      => 'Környékünk'
                ),
                'terkep'          => array(
                        'controller' => 'contact',
                        'action'     => 'map',
                        'label'      => 'Térkép'
                ),
                'kapcsolat'       => array(
                        'controller' => 'contact',
                        'action'     => 'mail',
                        'label'      => 'Kapcsolat'
                ),
                'videok'          => array(
                        'controller' => 'media',
                        'action'     => 'video',
                        'label'      => 'Videók'
                )
        ),
        'contactController' => array(
                'map'         => array(
                        'width'  => 970,
                        'height' => 600,
                        'src'    => 'http://maps.google.hu/maps?f=q&amp;source=embed&amp;'
                                    . 'hl=hu&amp;geocode=&amp;q=krisna-v%C3%B6lgy+somogyv%C3%A1mos&amp;'
                                    . 'sll=47.15984,19.500732&amp;sspn=4.796103,9.448242&amp;'
                                    . 'ie=UTF8&amp;hq=krisna-v%C3%B6lgy+somogyv%C3%A1mos&amp;'
                                    . 'hnear=&amp;cid=3669841666243940127&amp;ll=46.7248,17.677002&amp;'
                                    . 'spn=0.602493,0.628967&amp;z=9&amp;output=embed&amp;iwloc=near'
                ),
                'messageForm' => array(
                        'name'    => array(
                                'type'      => 'string',
                                'minLength' => 1,
                                'default'   => '',
                                'message'   => 'A nevet kötelező megadni'
                        ),
                        'email'   => array(
                                'type'    => 'email',
                                'default' => '',
                                'message' => 'Érvénytelen e-mail cím'
                        ),
                        'message' => array(
                                'type'      => 'string',
                                'minLength' => 1,
                                'default'   => '',
                                'message'   => 'Üzenetet kötelező írni'
                        )
                )
        ),
        'mediaController'   => array(
                'video' => array(
                        array(
                                'title' => 'Krisna-völgy ökofalu',
                                'src'   => '//www.youtube.com/embed/DdLYSENHERw'
                        ),
                        array(
                                'title' => 'Somogyvámoson a Krisna-hívők döntenek',
                                'src'   => '//embed.indavideo.hu/player/video/65ecf2d9bc/'
                        ),
                        array(
                                'title' => 'Vrindavan Ka Dulara: Little Krishna Hindi Film Trilogy DVD 1',
                                'src'   => '//www.youtube.com/embed/r7IctsmLopE'
                        )
                )
        ),
        'homeController'    => array(
                'event' => array(
                        array(
                                'title'       => 'Legelső Legelés – Tehénkihajtó Ünnep',
                                'date'        => '2015. április közepe',
                                'time'        => '10 órai kezdettel',
                                'description' => 'dátumot pontosítunk még'
                        ),
                        array(
                                'title'       => 'Virágom, virágom...  Májusi Virágünnep',
                                'date'        => '2015. június 14.',
                                'time'        => '10-17',
                                'description' => ''
                        ),
                        array(
                                'title'       => 'Krisna-völgyi Búcsú – Családi Élményfesztivál 3 napon át!',
                                'date'        => '2015 július 17-18-19.',
                                'time'        => '',
                                'description' => ''
                        ),
                        array(
                                'title'       => 'Krisna Szülinapja Fesztivál – Janmashtami',
                                'date'        => '2015. szeptember 5.',
                                'time'        => '10-21',
                                'description' => ''
                        )
                )
        )
);
