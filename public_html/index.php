<?php
use Mvc\Controller\Front as FrontController;

/**
 * Define path to application directory.
 *
 * @global string APPLICATION_PATH
 */
defined('APPLICATION_PATH')
|| define('APPLICATION_PATH', realpath(dirname(dirname((__FILE__)))));
/**
 * Define application environment.
 *
 * @global string APPLICATION_ENV
 */
defined('APPLICATION_ENV')
|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
/**
 * Set up error display.
 */
if ('development' === APPLICATION_ENV) {
    ini_set('display_errors', true);
}
/**
 * Typically, you will also want to add your library/ directory to the include_path.
 */
set_include_path(implode(PATH_SEPARATOR,
                         array(
                                 APPLICATION_PATH . '/library',
                                 APPLICATION_PATH . '/application/controller',
                                 get_include_path(),
                         )));
/**
 * register autoloader
 */
require_once(APPLICATION_PATH . '/library/Autoloader.php');
\Autoloader::getInstance()->register();
/**
 * Create application and run
 */
FrontController::getInstance()->setConfig(APPLICATION_PATH . '/application/config/' . APPLICATION_ENV . '.php')->run();
